from django.test import TestCase, Client, RequestFactory
from django.contrib.auth.models import User

class story9(TestCase):

	def test_template_used(self):
		response = Client().get('/story9/')
		self.assertTemplateUsed(response, 'story9/index.html')

	def test_list_url_is_resolved(self):
		response = Client().get('/story9/')
		self.assertEquals(response.status_code, 200)

	def test_tampilan_story9(self):
		response = Client().get('/story9/')
		isi = response.content.decode('utf8')
		self.assertIn('Login',isi)
		self.assertIn('Daftar',isi)

	def test_registerPage_else(self):
		test = 'Anonymous'
		response_post = Client().post('/story9/register/', {'username': test, 'password': test})
		self.assertEqual(response_post.status_code, 200)

	def test_loginPage_else(self):
		test = 'Anonymous'
		response_post = Client().post('/story9/login/', {'username': test, 'password': test})
		self.assertEqual(response_post.status_code, 200)

	def test_logout(self):
		test = 'Anonymous'
		self.client.login(username='test', password='test')
		response = self.client.get('/story9/logout/')
		self.assertEqual(response.status_code, 302)
