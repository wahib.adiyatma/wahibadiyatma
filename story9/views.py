from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm
from django.contrib import messages




def home(request):
	context = {
		'judul' : 'GetToKnow Wahib',
	}
	return render(request,'story9/index.html', context)

def loginPage(request):
 
    if request.user.is_authenticated:
        return redirect('story9:home')

    else:
        if request.method == "POST":
        	username_user = request.POST.get('username')
        	password_user = request.POST.get('password')
        	user = authenticate(request, username=username_user, password=password_user)
        	if user is not None:
        		login(request, user)
        		return redirect('story9:home')
        	else:
        		messages.info(request, 'Nama atau passoword yang anda masukkan salah')
    return render(request,'story9/login.html')

def logoutUser(request):
    logout(request)
    return redirect('story9:home')

def registerPage(request):
    if request.user.is_authenticated:
        return redirect('story9:home')

    else:    
        form = CreateUserForm()
        if request.method =="POST":
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, "Akun " + user + " berhasil dibuat")
                return redirect('story9:login')

    context = {'form':form}
    return render(request, 'story9/register.html', context)
