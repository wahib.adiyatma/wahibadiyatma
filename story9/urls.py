from django.conf.urls import url, include
from django.urls import path

from . import views
urlpatterns = [
    url(r'^$', views.home, name='home'),
    path('login/',views.loginPage, name='login'),
    path('logout/',views.logoutUser, name='logout'),
    path('register/',views.registerPage, name='register'),
]