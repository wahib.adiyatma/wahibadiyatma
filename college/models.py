from django.db import models

# Create your models here.
class CollegeModel(models.Model):
	LIST_SKS = (
		('1 SKS','1 SKS'),
		('2 SKS','2 SKS'),
		('2 SKS','3 SKS'),	
		('4 SKS','4 SKS'),				
		)
	LIST_SEMESTER = (
		('Gasal - 2017/2018','Gasal - 2017/2018'),
		('Genap - 2017/2018','Genap - 2017/2018'),
		('Gasal - 2018/2019','Gasal - 2018/2019'),
		('Genap - 2018/2019','Genap - 2018/2019'),			
		('Gasal - 2019/2020','Gasal - 2019/2020'),				
		('Genap - 2019/2020','Genap - 2019/2020'),
		('Gasal - 2020/2021','Gasal - 2020/2021'),
		('Genap - 2020/2021','Genap - 2020/2021'),
		)

	nama_matkul = models.CharField(max_length=30)
	nama_dosen = models.CharField(max_length=100)
	jumlah_sks = models.CharField(
			max_length=10,
			choices = LIST_SKS,
			default = '1 SKS',
		)

	deskripsi_matkul = models.TextField()

	semester = models.CharField(
			max_length=30,
			choices = LIST_SEMESTER,
			default = 'Gasal - 2020/2021',

		)	
	ruang_kelas = models.CharField(max_length=4)


	def __str__(self):
		return "{}.{}".format(self.id,self.nama_matkul)