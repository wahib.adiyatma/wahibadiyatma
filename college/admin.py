from django.contrib import admin

# Register your models here.
from .models import CollegeModel

admin.site.register(CollegeModel)