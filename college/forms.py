from django import forms
from .models import CollegeModel

class CollegeForm(forms.ModelForm):
	class Meta :
		model = CollegeModel
		fields = [
			'nama_matkul',
			'nama_dosen',
			'jumlah_sks',
			'deskripsi_matkul',
			'semester',
			'ruang_kelas',
		]

		widgets = {
		'nama_matkul' : forms.TextInput(
				attrs={
					'class':'form-control',
					'placeholder':'Masukan Mata Kuliah'
				}
			),
		'nama_dosen': forms.TextInput(
				attrs={
					'class':'form-control',
					'placeholder':'Masukan Mata Kuliah',
				}
			),
		'jumlah_sks' : forms.Select(
				attrs={
					'class':'form-control',
				}
		),
		'deskripsi_matkul' : forms.Textarea(
				attrs={
					'class':'form-control',
					'placeholder':'Deskripsikan Mata Kuliah Disini',
				}
			),
		'semester': forms.Select(
				attrs={
					'class':'form-control',
				}
			),
		'ruang_kelas': forms.TextInput(
				attrs={
					'class':'form-control',
					'placeholder':'Masukan 4 Digit Ruang Kelas'
				}
			)
		}
