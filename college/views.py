from django.shortcuts import render, redirect
from .forms import CollegeForm
from .models import CollegeModel

def index(request):
	college_form = CollegeForm(request.POST or None)

	if request.method == 'POST':
		if college_form.is_valid():
			college_form.save()
			return redirect('college:matkul')

	context = {
		'data_form': college_form,
		'judul' : 'Tambahkan Mata Kuliah',
	}
	return render(request,'college/index.html', context)


def update(request,update_id):
	matkul_update = CollegeModel.objects.get(id=update_id)

	data_update = {
		'nama_matkul' : matkul_update.nama_matkul,
		'nama_dosen' : matkul_update.nama_dosen,
		'jumlah_sks' : matkul_update.jumlah_sks,
		'deskripsi_matkul' : matkul_update.deskripsi_matkul,
		'semester' : matkul_update.semester,
		'ruang_kelas' : matkul_update.ruang_kelas, 
	}
	college_form = CollegeForm(request.POST or None, initial = data_update, instance=matkul_update)
	
	if request.method == 'POST':
		if college_form.is_valid():
			college_form.save()
			return redirect('college:detailmatkul')

	context = {
		'data_form': college_form,
		'judul' : 'Update Mata Kuliah',
	}
	return render(request,'college/update.html', context) 

def delete(request,delete_id):
	CollegeModel.objects.filter(id=delete_id).delete()
	return redirect('college:detailmatkul')

def matkul(request):
	data_matkul = CollegeModel.objects.all()

	context = {
		'data_matkul': data_matkul,
		'judul' : 'Mata Kuliah Wahib',
	}

	return render(request,'college/matkul.html',context)

def detail_matkul(request):
	data_matkul = CollegeModel.objects.all()
	context = {
		'data_matkul': data_matkul,
		'judul' : 'Detail Mata Kuliah Wahib',
	}

	return render(request,'college/detailmatkul.html',context)

def detailpermatkul(request, matkul_id):
	data_matkul = CollegeModel.objects.filter(id=matkul_id)
	context = {
		'data_matkul': data_matkul,
		'judul' : 'Detail Mata Kuliah',
	}

	return render(request,'college/detailpermatkul.html',context)


