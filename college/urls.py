from django.conf.urls import url, include

from . import views
urlpatterns = [
    url(r'^$', views.index, name="/"),
    url(r'^delete/(?P<delete_id>[0-9]+)$', views.delete,name='delete'),
    url(r'^update/(?P<update_id>[0-9]+)$', views.update,name='update'),
    url(r'^matkul/', views.matkul, name='matkul'),
    url(r'^detailmatkul/', views.detail_matkul, name='detailmatkul'),
    url(r'^detailpermatkul/(?P<matkul_id>[0-9]+)$', views.detailpermatkul, name='detailpermatkul'),
] 