from django.shortcuts import render

def index(request):
	context = {
		'judul' : 'GetToKnow Wahib',
	}
	return render(request,'index.html', context)