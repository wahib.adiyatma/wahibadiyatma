"""wahibadiyatma URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include

from . import views
urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^work/', include(('work.urls', 'work'),namespace='work')),
    url(r'^college/', include(('college.urls','college'),namespace='college')),
    url(r'^$', views.index, name='index'),
    path('meetme/', include(('meetme.urls', 'meetme'), namespace ='meetme')),
    path('searchBooks/', include(('searchBooks.urls', 'searchBooks'), namespace ='searchBooks')),
    path('story9/', include(('story9.urls', 'story9'), namespace ='story9')),
]
