from django.apps import AppConfig


class SearchbooksConfig(AppConfig):
    name = 'searchBooks'
