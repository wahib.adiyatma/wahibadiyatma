from django.test import TestCase, Client

class searchBooks(TestCase):

	def test_template_used(self) :
		response = Client().get('/searchBooks/')
		self.assertTemplateUsed(response, 'searchBooks/index.html')

	def test_list_url_is_resolved(self) :
		response = Client().get('/searchBooks/')
		self.assertEquals(response.status_code, 200)

	def test_tampilan_searchBooks(self):
		response = Client().get('/searchBooks/')
		isi = response.content.decode('utf8')
		self.assertIn('<button id="keyword" class="btn btn-primary my-2" type="button" style="margin-left:45%;">Search</button>',isi)
		self.assertIn('<input id="input" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">',isi)

	def test_searchBooks_url_is_resolved(self):
		response = Client().get('/searchBooks/data/?q=')
		self.assertEquals(response.status_code, 200)

	def test_searchBooks_url_is_exist(self):
		response = Client().get('/searchBooks/data/?q=sherlock')
		isi = response.content.decode('utf8')
		self.assertIn('Sherlock Holmes',isi)

