$('#keyword').click(function(){
	var input_user = $("#input").val();
	$('.table').removeClass('tampilkan')

	$.ajax({
		url : '/searchBooks/data?q='+input_user,
		success: function(data){
			var data_items = data.items;
			$("#body_hasil_pencarian").empty();
			for ( i = 0; i < data_items.length; i++){
				var count = i+1;
				var judul = data_items[i].volumeInfo.title;
				var gambar = data_items[i].volumeInfo.imageLinks.smallThumbnail;
				var pengarang = data_items[i].volumeInfo.authors;
				var penerbit = data_items[i].volumeInfo.publisher;
				var deskripsi = data_items[i].volumeInfo.description;
				$('#body_hasil_pencarian').append('<tr><th scope="row">'+count+'</th><td><img src="'+gambar+'"></td><td>'+judul+'</td>'+'<td>'+pengarang+'</td>'+'<td>'+penerbit+'</td>'+'<td>'+deskripsi+'</td>');
			}
		}
	})
});
