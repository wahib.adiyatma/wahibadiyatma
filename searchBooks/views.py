from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

def index(request):
	context = {
		'judul' : 'GetToKnow Wahib',
	}
	return render(request,'searchBooks/index.html', context)
# Create your views here.

def data(request):
	url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
	req = requests.get(url)
	data = json.loads(req.content)
	return JsonResponse(data, safe=False)