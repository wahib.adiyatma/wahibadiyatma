/*===== MENU SHOW =====*/
const showMenu = (toggleId, navId) =>{
    const toggle = document.getElementById(toggleId),
    nav = document.getElementById(navId)

    if(toggle && nav){
        toggle.addEventListener('click', ()=>{
            nav.classList.toggle('show')
        })
    }
}
showMenu('nav-toggle','nav-menu')

/*===== ACTIVE AND REMOVE MENU =====*/
const navLink = document.querySelectorAll('.nav__link');

function linkAction(){
  /*Active link*/
  navLink.forEach(n => n.classList.remove('active'));
  this.classList.add('active');

  /*Remove menu mobile*/
  const navMenu = document.getElementById('nav-menu')
  navMenu.classList.remove('show')
}
navLink.forEach(n => n.addEventListener('click', linkAction));

/*===== SCROLL REVEAL ANIMATION =====*/
const sr = ScrollReveal({
    origin: 'top',
    distance: '80px',
    duration: 2000,
    reset: true
});

/*SCROLL HOME*/
sr.reveal('.home__title',{});
sr.reveal('.button',{delay: 200});
sr.reveal('.home__img',{delay: 400});
sr.reveal('.home__social-icon',{ interval: 200});

/*SCROLL ABOUT*/
sr.reveal('.about__img',{});
sr.reveal('.about__subtitle',{delay: 400});
sr.reveal('.about__text',{delay: 400});

/*SCROLL HOBBIES*/
sr.reveal('.hobbies__img',{});
sr.reveal('.hobbies__subtitle',{delay: 400});
sr.reveal('.hobbies__text',{delay: 400});

/*SCROLL SKILLS*/
sr.reveal('.skills__subtitle',{});
sr.reveal('.skills__text',{});
sr.reveal('.skills__data',{interval: 200});
sr.reveal('.skills__img',{delay: 600});

/*SCROLL WORK*/
sr.reveal('.background__btn',{interval: 200});
sr.reveal('.work__img',{interval: 200});

/* BUTTON WORK */
const button1 = document.getElementById("bg__btn__univ");
const list1 = document.getElementById("bg__lists__univ");

/*SCROLL CONTACT*/
sr.reveal('.section-title',{interval: 200});
sr.reveal('.contact__container',{delay: 400});
sr.reveal('.iconlinkedin',{delay: 400});
sr.reveal('.iconmedium',{delay: 600});
sr.reveal('.iconblog',{delay: 800});
sr.reveal('.icongithub',{delay: 1000});

list1.style.display = "none";
button1.addEventListener("click", (event) => {
  if(list1.style.display == "none"){
    list1.style.display = "block";
  }else{
    list1.style.display = "none";
  }
})

const button2 = document.getElementById("bg__btn__hs");
const list2 = document.getElementById("bg__lists__hs");

list2.style.display = "none";
button2.addEventListener("click", (event) => {
  if(list2.style.display == "none"){
    list2.style.display = "block";
  }else{
    list2.style.display = "none";
  }
})


